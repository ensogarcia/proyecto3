package pe.edu.tecsup.semana03;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView List;
    private ArrayList ImageList;
    public ArrayList NameList;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        NameList = new ArrayList<String>();
        ImageList = new ArrayList<Integer>();

        ImageList.add(R.drawable.bulbasaur);
        ImageList.add(R.drawable.ivysaur);
        ImageList.add(R.drawable.venusaur);
        ImageList.add(R.drawable.charmander);
        ImageList.add(R.drawable.charmeleon);
        ImageList.add(R.drawable.charizard);
        ImageList.add(R.drawable.squirtle);
        ImageList.add(R.drawable.wartortle);
        ImageList.add(R.drawable.blastoise);


        NameList.add("bulbasaur");
        NameList.add("ivysaur");
        NameList.add("venusaur");
        NameList.add("charmander");
        NameList.add("charmeleon");
        NameList.add("charizard");
        NameList.add("squirtle");
        NameList.add("wartortle");
        NameList.add("blastoise");

        List = findViewById(R.id.MainList);
        List.setAdapter(new CustomAdapter());


        List.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

               Toast.makeText(MainActivity.this, "" + NameList.get(position), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public class CustomAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return 9;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            view = getLayoutInflater().inflate(R.layout.item_list, null);
            TextView Name= view.findViewById(R.id.ImageName);
            ImageView Image= view.findViewById(R.id.ImageList);
            Name.setText(NameList.get(position).toString());
            Image.setImageDrawable(getResources().getDrawable((int)ImageList.get(position)));
            return view;
        }
    }
}
